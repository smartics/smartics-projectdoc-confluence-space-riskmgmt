projectdoc Risk Management Doctypes Add-on
==========================================

Deprecated: New version of this project is found at https://bitbucket.org/smartics/smartics-doctype-model-riskmgmt

##Overview

This is a free add-on for [projectdoc](https://www.smartics.eu/confluence/display/PDAC1/) for Confluence.

The add-on provides the blueprints to create pages for

  * Risks
  * Risk Phase
  * Risk Probability
  * Risk Severity
  * Risk Type

It also provides space blueprints to get started with your documentation project quickly.

##Fork me!
Feel free to fork this project to adjust the templates according to your project requirements.

The projectdoc Risk Management Doctypes Add-on is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

##Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/display/PDAC1/Doctypes+for+Risk+Management)
  * Coming soon: the [add-on on the Atlassian Marketplace](https://marketplace.atlassian.com/plugins/de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-riskmgmt)