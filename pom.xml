<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>de.smartics.atlassian.confluence</groupId>
  <artifactId>smartics-projectdoc-confluence-space-riskmgmt</artifactId>
  <version>1.0.0-SNAPSHOT</version>
  <packaging>atlassian-plugin</packaging>

  <name>projectdoc for Risk Management</name>
  <description>
    projectdoc Blueprints for risk managing.
  </description>
  <inceptionYear>2014</inceptionYear>

  <organization>
    <name>smartics</name>
    <url>http://www.smartics.de/</url>
  </organization>
  <licenses>
    <license>
      <name>The Apache Software License, Version 2.0</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0</url>
      <distribution>repo</distribution>
      <comments>Copyright 2014-2018 Kronseder &amp; Reiner GmbH, smartics</comments>
    </license>
  </licenses>

  <developers>
    <developer>
      <id>robert.reiner</id>
      <name>Robert Reiner</name>
      <url>https://www.smartics.de/go/robertreiner</url>
      <organization>Kronseder &amp; Reiner GmbH, smartics</organization>
      <organizationUrl>http://www.smartics.de/</organizationUrl>
    </developer>
    <developer>
      <id>anton.kronseder</id>
      <name>Anton Kronseder</name>
      <url>https://www.smartics.de/go/antonkronseder</url>
      <organization>Kronseder &amp; Reiner GmbH, smartics</organization>
      <organizationUrl>http://www.smartics.de/</organizationUrl>
    </developer>
  </developers>

  <prerequisites>
    <maven>3.0.3</maven>
  </prerequisites>

  <scm>
    <connection>scm:git:https://bitbucket.org/smartics/${project.artifactId}</connection>
    <developerConnection>scm:git:https://bitbucket.org/smartics/${project.artifactId}</developerConnection>
    <url>https://bitbucket.org/smartics/${project.artifactId}</url>
    <tag>HEAD</tag>
  </scm>

  <issueManagement>
    <system>jira</system>
    <url>https://www.smartics.eu/jira/projects/PDPRJMGMT</url>
  </issueManagement>

  <distributionManagement>
    <repository>
      <id>${project-visibility}</id>
      <name>internal smartics release repository</name>
      <url>${repo.server.upload}/${project-visibility}</url>
    </repository>
    <snapshotRepository>
      <uniqueVersion>false</uniqueVersion>
      <id>${project-visibility}-snapshot</id>
      <name>internal smartics snapshot repository</name>
      <url>${repo.server.upload}/${project-visibility}-snapshot</url>
    </snapshotRepository>
<!--     <site>
      <id>${project-visibility}-site</id>
      <name>documentation site</name>
      <url>${site.url}</url>
    </site>
 -->
  </distributionManagement>

  <properties>
    <project-documentation-root-url>http://www.smartics.eu/confluence/display/PDAC1</project-documentation-root-url>
    <project-visibility>corporation</project-visibility>

    <confluence.version>5.10.8</confluence.version>
    <amps.version>6.2.9</amps.version>
    <version.quick.reload>1.29</version.quick.reload>

    <jdk.version>1.7</jdk.version>
    <confluence.data.version>${confluence.version}</confluence.data.version>
    <confluence-editor.version>${confluence.version}</confluence-editor.version>
    <refapp.version>${confluence.version}</refapp.version>

    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

    <server.name>www.smartics.eu</server.name>
    <build.server.name>www.smartics.eu</build.server.name>
    <!-- The server prefix is used by projects with modules. -->
    <repo.server>https://${server.name}/nexus</repo.server> <!-- HTTP -->
    <repo.server.prefix>${repo.server}/content</repo.server.prefix>
    <repo.server.upload>dav:${repo.server.prefix}/repositories</repo.server.upload>
    <scm.url>scm:svn:https://${build.server.name}/svn/public</scm.url>

    <!-- The following properties are accessible from site documentation and therefore contain no dots. -->
    <webroot>https://${build.server.name}</webroot>
    <weburl>${webroot}/${project.groupId}/${project.artifactId}/${project.version}</weburl>
    <scm-weburl>https://${build.server.name}/svn/public</scm-weburl> <!-- No subdir ${project-visibility} currently. -->
    <repo-url>https://${server.name}/nexus</repo-url> <!-- HTTPs -->
    <repo-download-url>${repo-url}/service/local/artifact/maven/redirect</repo-download-url>

    <buildmetadata-maven-plugin.version>1.6.0</buildmetadata-maven-plugin.version>

    <maven-compiler-plugin.version>3.1</maven-compiler-plugin.version>
    <maven-clean-plugin.version>2.6.1</maven-clean-plugin.version>
    <maven-dependency-plugin.version>2.10</maven-dependency-plugin.version>
    <maven-resources-plugin.version>2.7</maven-resources-plugin.version>
    <maven-deploy-plugin.version>2.8.2</maven-deploy-plugin.version>
    <maven-jar-plugin.version>2.6</maven-jar-plugin.version>
    <maven-release-plugin.version>2.5.2</maven-release-plugin.version>
    <maven-install-plugin.version>2.5.2</maven-install-plugin.version>
    <maven-scm-plugin.version>1.9.4</maven-scm-plugin.version>
    <maven-site-plugin.version>3.4</maven-site-plugin.version>
    <maven-javadoc-plugin.version>2.10.3</maven-javadoc-plugin.version>
    <maven-source-plugin.version>2.4</maven-source-plugin.version>
    <maven-site-plugin.version>3.4</maven-site-plugin.version>

    <maven-license-plugin.version>1.9.0</maven-license-plugin.version>
    <maven-site-plugin.version>3.4</maven-site-plugin.version>

    <smartics-projectdoc-confluence.version>2.0.0-SNAPSHOT</smartics-projectdoc-confluence.version>
    <smartics-projectdoc-confluence-space-core.version>8.0.0-SNAPSHOT</smartics-projectdoc-confluence-space-core.version>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>com.atlassian.confluence</groupId>
        <artifactId>confluence-plugins-platform-pom</artifactId>
        <version>${confluence.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>
    <dependencies>
    <dependency>
      <groupId>com.atlassian.confluence</groupId>
      <artifactId>confluence</artifactId>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>com.atlassian.confluence.plugins</groupId>
      <artifactId>confluence-create-content-plugin</artifactId>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>de.smartics.atlassian.confluence</groupId>
      <artifactId>smartics-projectdoc-confluence</artifactId>
      <version>${smartics-projectdoc-confluence.version}</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>com.atlassian.confluence.plugins</groupId>
      <artifactId>confluence-templates</artifactId>
      <version>${confluence.version}</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>com.atlassian.confluence.plugins</groupId>
      <artifactId>confluence-editor</artifactId>
      <version>${confluence-editor.version}</version>
      <type>atlassian-plugin</type>
    </dependency>

    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>servlet-api</artifactId>
      <version>2.3</version>
      <scope>provided</scope>
    </dependency>
  </dependencies>

  <build>
    <extensions>
      <extension>
        <groupId>org.apache.maven.wagon</groupId>
        <artifactId>wagon-webdav</artifactId>
        <version>1.0-beta-2</version>
      </extension>
    </extensions>

    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-clean-plugin</artifactId>
          <version>${maven-clean-plugin.version}</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-dependency-plugin</artifactId>
          <version>${maven-dependency-plugin.version}</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-resources-plugin</artifactId>
          <version>${maven-resources-plugin.version}</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-deploy-plugin</artifactId>
          <version>${maven-deploy-plugin.version}</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-jar-plugin</artifactId>
          <version>${maven-jar-plugin.version}</version>
          <configuration>
            <index>true</index>
            <archive>
              <manifest>
                <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                <addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
              </manifest>
              <manifestEntries>
                <Implementation-URL>${project.url}</Implementation-URL>
                <Built-OS>${os.name} / ${os.arch} / ${os.version}</Built-OS>
                <Maven-Version>${build.maven.version}</Maven-Version>
                <Java-Version>${java.version}</Java-Version>
                <Java-Vendor>${java.vendor}</Java-Vendor>
              </manifestEntries>
            </archive>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-install-plugin</artifactId>
          <version>${maven-install-plugin.version}</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-release-plugin</artifactId>
          <version>${maven-release-plugin.version}</version>
          <configuration>
            <useReleaseProfile>false</useReleaseProfile>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-scm-plugin</artifactId>
          <version>${maven-scm-plugin.version}</version>
          <configuration>
            <connectionType>developerConnection</connectionType>
          </configuration>
        </plugin>

        <plugin>
          <groupId>org.eclipse.m2e</groupId>
          <artifactId>lifecycle-mapping</artifactId>
          <version>1.0.0</version>
          <configuration>
            <lifecycleMappingMetadata>
              <pluginExecutions>
                <pluginExecution>
                  <pluginExecutionFilter>
                    <groupId>de.smartics.maven.plugin</groupId>
                    <artifactId>buildmetadata-maven-plugin</artifactId>
                    <versionRange>[0.0.0,)</versionRange>
                    <goals>
                      <goal>provide-buildmetadata</goal>
                    </goals>
                  </pluginExecutionFilter>
                  <action>
                    <ignore />
                  </action>
                </pluginExecution>
                <pluginExecution>
                  <pluginExecutionFilter>
                    <groupId>com.atlassian.maven.plugins</groupId>
                    <artifactId>maven-confluence-plugin</artifactId>
                    <versionRange>[0.0.0,)</versionRange>
                    <goals>
                      <goal>compress-resources</goal>
                      <goal>generate-test-manifest</goal>
                      <goal>copy-bundled-dependencies</goal>
                      <goal>copy-test-bundled-dependencies</goal>
                      <goal>filter-plugin-descriptor</goal>
                      <goal>filter-test-plugin-descriptor</goal>
                      <goal>generate-manifest</goal>
                      <goal>generate-rest-docs</goal>
                    </goals>
                  </pluginExecutionFilter>
                  <action>
                    <ignore />
                  </action>
                </pluginExecution>
              </pluginExecutions>
            </lifecycleMappingMetadata>
          </configuration>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>${maven-compiler-plugin.version}</version>
          <configuration>
            <source>${jdk.version}</source>
            <target>${jdk.version}</target>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
    <plugins>
      <plugin>
        <groupId>com.atlassian.maven.plugins</groupId>
        <artifactId>maven-confluence-plugin</artifactId>
        <version>${amps.version}</version>
        <extensions>true</extensions>
        <configuration>
          <productVersion>${confluence.version}</productVersion>
          <productDataVersion>${confluence.data.version}</productDataVersion>

          <httpPort>1990</httpPort>

          <!-- make AMPS faster -->
          <enableFastdev>false</enableFastdev>
          <useFastdevCli>false</useFastdevCli>
          <enableDevToolbox>false</enableDevToolbox>
          <enablePde>false</enablePde>

          <skipRestDocGeneration>true</skipRestDocGeneration>
          <allowGoogleTracking>false</allowGoogleTracking>
          <skipManifestValidation>true</skipManifestValidation>
          <extractDependencies>false</extractDependencies>
          <skipManifestValidation>true</skipManifestValidation>
          <enableQuickReload>true</enableQuickReload>
          <skipTests>true</skipTests>

          <jvmArgs>-Xms2048m -Xmx2048m</jvmArgs>

          <systemPropertyVariables>
            <localRepository>${settings.localRepository}</localRepository>

            <http.confluence.port>1990</http.confluence.port>
            <context.confluence.path>/confluence</context.confluence.path>
            <baseurl.confluence>http://localhost:${http.confluence.port}${context.confluence.path}</baseurl.confluence>

            <atlassian.webresource.disable.minification>true</atlassian.webresource.disable.minification>
            <atlassian.dev.mode>true</atlassian.dev.mode>
            <!--  Due to: https://jira.atlassian.com/browse/CONF-24070 -->
            <confluence.velocity.deprecation.strictmode>false</confluence.velocity.deprecation.strictmode>
          </systemPropertyVariables>

          <instructions>
            <Import-Package>
              <!-- com.atlassian.confluence.*;version="0.0.0", -->
              de.smartics.projectdoc.atlassian.confluence;version="0.0.0",*;resolution:=optional
            </Import-Package>
          </instructions>
          <pluginArtifacts>
            <pluginArtifact>
              <groupId>de.smartics.atlassian.confluence</groupId>
              <artifactId>smartics-projectdoc-confluence</artifactId>
              <version>${smartics-projectdoc-confluence.version}</version>
            </pluginArtifact>
            <pluginArtifact>
              <groupId>de.smartics.atlassian.confluence</groupId>
              <artifactId>smartics-projectdoc-confluence-space-core</artifactId>
              <version>${smartics-projectdoc-confluence-space-core.version}</version>
            </pluginArtifact>
            <pluginArtifact>
              <groupId>com.atlassian.labs.plugins</groupId>
              <artifactId>quickreload</artifactId>
              <version>${version.quick.reload}</version>
            </pluginArtifact>
          </pluginArtifacts>
        </configuration>
      </plugin>
      <plugin>
        <groupId>de.smartics.maven.plugin</groupId>
        <artifactId>buildmetadata-maven-plugin</artifactId>
        <version>${buildmetadata-maven-plugin.version}</version>
        <executions>
          <execution>
            <phase>initialize</phase>
            <goals>
              <goal>provide-buildmetadata</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <buildDatePattern>dd.MM.yyyy HH:mm:ss</buildDatePattern>
        </configuration>
      </plugin>

      <plugin>
        <groupId>com.mycila.maven-license-plugin</groupId>
        <artifactId>maven-license-plugin</artifactId>
        <version>${maven-license-plugin.version}</version>
        <executions>
          <execution>
            <goals>
              <goal>check</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <strictCheck>true</strictCheck>
          <header>src/etc/header.txt</header>
          <headerDefinitions>
            <headerDefinition>src/etc/javadoc.xml</headerDefinition>
          </headerDefinitions>
          <properties>
            <year>${build.copyright.year}</year>
          </properties>
          <excludes>
            <exclude>**/.fbprefs</exclude>
            <exclude>**/.pmd</exclude>
            <exclude>**/.checkstyle</exclude>
            <exclude>**/.ruleset</exclude>
            <exclude>**/COPYRIGHT.txt</exclude>
            <exclude>**/LICENSE</exclude>
            <exclude>**/LICENSE.txt</exclude>
            <exclude>**/LICENSE-*</exclude>
            <exclude>**/*.xcf</exclude>
            <exclude>**/javadoc.xml</exclude>
            <exclude>**/header.txt</exclude>
            <exclude>**/xhtml/xhtml1-transitional.dtd</exclude>
            <exclude>**/*.svg</exclude>
            <exclude>**/*.eot</exclude>
            <exclude>**/*.woff</exclude>
            <exclude>**/*.ttf</exclude>
            <exclude>**/*.soy</exclude>
            <exclude>**/*.pdf</exclude>
            <exclude>**/pom.xml.releaseBackup</exclude>
            <exclude>.gitignore</exclude>
            <exclude>**/*.md</exclude>
          </excludes>
          <mapping>
            <vm>XML_STYLE</vm>
          </mapping>
        </configuration>
      </plugin>
      <plugin>
        <artifactId>maven-resources-plugin</artifactId>
        <executions>
          <execution>
            <id>copy-resources</id>
            <phase>compile</phase>
            <goals>
              <goal>copy-resources</goal>
            </goals>
            <configuration>
              <outputDirectory>${project.build.outputDirectory}/META-INF</outputDirectory>
              <resources>
                <resource>
                  <directory>${basedir}</directory>
                  <includes>
                    <include>COPYRIGHT.txt</include>
                    <include>LICENSE-*</include>
                  </includes>
                  <filtering>false</filtering>
                </resource>
              </resources>
            </configuration>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
</project>
